package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
public class stepDefinition {

    @Given("^User is on NetBanking landing page$")
    public void user_is_on_netbanking_landing_page() throws Throwable {
        // code to navigate to login url
        System.out.println("navigate to login url");
    }

    /*@When("^User login into application with username and password$")
    public void user_login_into_application_with_username_and_password() throws Throwable {
        // code to login
        System.out.println("lloged in success");
    }*/

    @When("^User login into application with \"([^\"]*)\" username and password \"([^\"]*)\"$")
    public void user_login_into_application_with_something_username_and_password_something(String strArg1, String strArg2) throws Throwable {
        System.out.println("Logged in success with parameters: " + strArg1 + " - " + strArg2);
    }


    @Then("^Home page is populated$")
    public void home_page_is_populated() throws Throwable {
        //code to home page validation
        System.out.println("Validated home page");
    }

    @And("^Cards displayed are \"([^\"]*)\"$")
    public void cards_displayed_are_something(String strArg1) throws Throwable {
        System.out.println("Validated cards: " + strArg1);
    }
}
